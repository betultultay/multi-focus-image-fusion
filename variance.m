function variance = variance(inputMatrix)
v = inputMatrix(:);
variance = var(v);
end

