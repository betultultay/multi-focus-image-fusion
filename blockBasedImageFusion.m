function outputImage = blockBasedImageFusion(inputImage1,inputImage2,compFunction,m,n)
%This function takes 2 images, merge them and turns a sharp image as an output.
%This function takes 2 images and splits them into blocks. Then by comparing the same areas
%gets the sharper block and paste it to another matrix. Thus returns the
%final matrix as an output image.
    inputImageGray1 = rgb2gray(inputImage1);
    inputImageGray2 = rgb2gray(inputImage2);
    inputImageGray1 = double(inputImageGray1);
    inputImageGray2 = double(inputImageGray2);
    splitImage1 = mat2tiles(inputImageGray1,[m,n]);
    splitImage2 = mat2tiles(inputImageGray2,[m,n]);
    [x,y] = size(splitImage1);
    M = splitImage1;
if compFunction == "Tenengrad"
    for i = 1:x
        for j = 1:y
            A = splitImage1{i,j};
            B = splitImage2{i,j};
            a = size(A(:));
            b = size(B(:));
            if a(1,1) == 9 && b(1,1) == 9
            gradian1 = tenengrad(A);
            gradian2 = tenengrad(B);
            if(gradian2 > gradian1)
                M{i,j} = splitImage2{i,j};
            elseif( gradian1 >= gradian2)
                M{i,j} = splitImage1{i,j};
            end
            end
        end
    end
end
%debug it and see which one is getting which values and sizes
if compFunction == "Variance"
    for i = 1:x
        for j = 1:y
            A = splitImage1{i,j};
            B = splitImage2{i,j};
            a = size(A(:));
            b = size(B(:));
            if a(1,1) == 9 && b(1,1) == 9
            variance1 = variance(A);
            variance2 = variance(B);
            if(variance2 > variance1)
                M{i,j}= splitImage2{i,j};
            elseif(variance1 >= variance2)
                M{i,j} = splitImage1{i,j};
            end
            end
        end
    end
end
M = cell2mat(M);
M = uint8(M);
%Coloring the image
inputImageGray1 = uint8(inputImageGray1);
inputImageGray2 = uint8(inputImageGray2);
originalGrayImage = ((inputImageGray1)/2) + ((inputImageGray2)/2);
gray = originalGrayImage - M;
red = (inputImage1(:,:,1))/2 + (inputImage2(:,:,1))/2;
green = (inputImage1(:,:,2))/2 + (inputImage2(:,:,2))/2;
blue = (inputImage1(:,:,3))/2 + (inputImage2(:,:,3))/2;
redChannel = red - (gray);
greenChannel = green - (gray);
blueChannel = blue - (gray);
finalImage = cat(3,redChannel,greenChannel,blueChannel);
outputImage = finalImage;
imshow(outputImage);
end