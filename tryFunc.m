function [coloredImage] = tryFunc(image1,image2,outputGrayImage)
%UNTİTLED3 Summary of this function goes here
%   Detailed explanation goes here
%originalGrayImage = rgb2gray(originalImage);
image1Gray = rgb2gray(image1);
image2Gray = rgb2gray(image2);
originalGrayImage = ((image1Gray)/2) + ((image2Gray)/2);
gray = originalGrayImage - outputGrayImage;
red = (image1(:,:,1))/2 + (image2(:,:,1))/2;
green = (image1(:,:,2))/2 + (image2(:,:,2))/2;
blue = (image1(:,:,3))/2 + (image2(:,:,3))/2;
redChannel = red - (gray);
greenChannel = green - (gray);
blueChannel = blue - (gray);
finalImage = cat(3,redChannel,greenChannel,blueChannel);
figure; imshow(finalImage);
coloredImage = finalImage;
end

