function [coloredImage] = grayscale2rgb(originalImage,outputGrayImage)
%UNTİTLED Summary of this function goes here
%   Detailed explanation goes here
originalGrayImage = rgb2gray(originalImage);
gray = originalGrayImage - outputGrayImage;
redChannel = originalImage(:,:,1) + (gray);
greenChannel = originalImage(:,:,2) + (gray);
blueChannel = originalImage(:,:,3) + (gray);
finalImage = cat(3,redChannel,greenChannel,blueChannel);
imshow(finalImage);
coloredImage = finalImage;
end

