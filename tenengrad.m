function [gradian,GX,GY] = tenengrad(inputMatrix)

A = [+1 0 -1; +2 0 -2; +1 0 -1];
B = [+1 +2 +1; 0 0 0; -1 -2 -1];
[i,j] = size(inputMatrix);
 for i = 1:i
        for j = 1:j
            X = inputMatrix{i,j};
                GX = sum(sum(A * X));
                GY = sum(sum(B * X)); 
                G = (GX)^2 + (GY)^2; 
                gradian = sqrt(G);
        end
 end 
end

